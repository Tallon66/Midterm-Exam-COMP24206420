# Statement of Originality
## COMP2420, Semester 1, 2023
### Australian National University

## Declaration
  I declare that everything that I have submitted in this assignment is entirely my own work, with the following exceptions:

In the fields below, replace 'N/A' with appropriate words, in case this applies
to you.  'N/A' stands for 'Not Applicable', which implies that this does not
apply to you (this is the default).  IF this does not apply to you, then leave the 'N/A' as is.


Add as many "url+licence+comment" entries as necessary

### Code:
If you copied (or slightly changed) code from a friend or external website (for example), you must reference it here. Even if you change the variable name or slightly change the functions, it must still be referenced here.


  - comment: N/A
    url: N/A
    licence: N/A


Add as many "url+licence+comment" entries as necessary

### Assets:
If you use any Python packages (other than the ones already used in the course: pandas, numpy, matplotlib, sklearn, sqlite3), it must be referenced here. 


  - comment: N/A
    url: N/A
    licence: N/A


### References:
If you use any external sources to increase your understanding of a topic, or to provide as evidence for a written question, it must be referenced here. If you want to in-text reference something, it must be referenced here. Harvard or APA is expected.


## Sign *your* name and uid here.

name1:  
uid1:
